axis         = require 'axis'
rupture      = require 'rupture'
autoprefixer = require 'autoprefixer-stylus'
js_pipeline  = require 'js-pipeline'
browserify   = require 'roots-browserify'
css_pipeline = require 'css-pipeline'
vueify       = require 'vueify'
coffeeify    = require 'coffeeify'


module.exports =
  ignores: ['readme.md', '**/layout.*', '**/_*', '.gitignore', 'ship.*conf']

  extensions: [
    js_pipeline
      files: ['assets/js/*.js']

    browserify
      files: 'assets/js/main.coffee'
      out: 'app.js'
      transforms: [coffeeify, vueify]


      # opts:
      #   fast: true
      #   insertGlobals: true
      # fast: true
      # insertGlobals: true

    css_pipeline(files: 'assets/css/*.styl')
  ]

  stylus:
    use: [axis(), rupture(), autoprefixer()]
    sourcemap: true

  'coffee-script':
    sourcemap: true

  jade:
    pretty: true
