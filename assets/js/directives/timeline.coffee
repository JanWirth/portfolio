gridSize = 16
yearHeight = 4 * 2 * gridSize # demi-section size * 2 sections * grid

calculateHeight = (duration) ->
  'height: ' + (yearHeight * duration) + 'px'

calculateTop = (end) ->
  'top: ' + (yearHeight * (end)) + 'px'

calculateSection = (duration, end = 0) ->
  calculateHeight(duration) + ';' + calculateTop(end)

module.exports.period =
  params: ['end']
  bind: ->
    @el.setAttribute 'style', calculateTop(16.5 - @params.end) #current year

module.exports.line =
  params: ['duration', 'end']
  bind: ->
    @el.setAttribute 'style', calculateSection @params.duration, @params.end
