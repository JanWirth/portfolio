Vue.use VueRouter

router = new VueRouter()

intro     = require './components/intro.vue'
vita      = require './components/vita.vue'
personal  = require './components/personal.vue'
portfolio = require './components/portfolio.vue'
contact   = require './components/contact.vue'

App = Vue.extend
  route:
    activate: (transition) ->
      console.log transition
  data: ->

    data =
      auth:
        fullAccess: true

    if @$route.path == '/Bewerbung-Daimler-UI-Designer'
      data.auth.fullAccess = true

    return data


Vue.component 'personal-info', personal

router.map

  '/Bewerbung-Daimler-UI-Designer':
    component: intro
    title: 'Motivation'

  '/':
    component: vita
    title: 'Vita'

  '/Vita':
    component: vita
    title: 'Vita'

  '/CV':
    component: vita
    title: 'Vita'

  '/Portfolio':
    component: portfolio
    title: 'Portfolio'

  '/Kontakt':
    component: contact
    title: 'Kontakt'

router.start App, '#App'
